### Problem Statement 23

You are given an array A of k integers in sequence from 0 to k - 1. You can perform the following two “moves” on the array:


Right shift: The value of i<sup>th</sup> element in the array moves to the i<sup>th</sup> + 1 position for all i ranging from 0 to k - 2. The value in position k - 1 moves to the 0<sup>th</sup> position.


Swap the values in adjacent cells (one swap counts as one “move”; multiple swaps are multiple moves)


After performing j such “moves”, you have a new array B. You give this array B to your friend and tasks him with finding out the smallest number of “moves” j that transforms the array A to B.


**Example:**


If you perform a single right shift move on the array [0,1,2,3], you get the array [3,0,1,2]

Or you can swap the 1st and 2nd positions for example [0,1,2,3] transforms to [0,2,1,3].


**Input format**

The first line of input consists of the integer T which is the number of test cases. Then T lines follow with the format as follows:


K a<sub>0</sub> a<sub>1</sub> … a<sub>K-1</sub>


Here the a<sub>i</sub>s represent the final array B i.e. B[i] = a<sub>i</sub>


K is the number of elements in the array.


**Output Format**

For each test case, output the minimum number of moves needed to transform A to B.


**Constraints**

1 <= T <= 10000

1 <= K <= 10^9


**Sample Input**
```
1
4 0 2 1 3
```

**Sample Output**
```
1
```